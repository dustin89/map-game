# Map Game
This is simple implementation of dungeon crawling game
## How to run
To start game run main method from Application class.

## Usage
By default initial room is set to a0. After game start there is a list of available moves printed on to the screen. 
To move to other room, type direction - n, e, w or s. Type 'exit' to close application


