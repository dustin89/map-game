package com.krygier.game;

import com.krygier.game.input.InputProvider;

import java.util.Map;

import static java.util.Objects.isNull;

public class Game {

    private static final String EXIT_COMMAND = "exit";

    private final InputProvider keyReader;

    public Game(InputProvider keyReader) {
        this.keyReader = keyReader;
    }

    public void play(Map<String, Map<String, String>> data, String firstRoom) {
        String currentRoom = firstRoom;
        String currentMove = null;

        while (isNull(currentMove) || !currentMove.equals(EXIT_COMMAND)) {

            Map<String, String> moves = data.get(currentRoom);
            if (isNull(moves)) {
                System.out.println("No moves found for room: " + currentRoom);
                continue;
            }
            System.out.println("Moves for room " + currentRoom + ": " + moves + ". Type direction or 'exit' to close application");
            currentMove = keyReader.provideInput();
            String newRoom = moves.get(currentMove);
            if (isNull(newRoom) && !EXIT_COMMAND.equals(currentMove)) {
                System.out.println("Move to " + currentMove + " is not available");
                continue;
            }
            currentRoom = newRoom;
        }

    }

}
