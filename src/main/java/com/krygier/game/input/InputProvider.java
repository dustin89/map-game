package com.krygier.game.input;

public interface InputProvider {
    String provideInput();
}
