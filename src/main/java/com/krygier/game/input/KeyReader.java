package com.krygier.game.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KeyReader implements InputProvider {

    @Override
    public String provideInput() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
