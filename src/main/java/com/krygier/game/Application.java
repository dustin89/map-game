package com.krygier.game;

import com.krygier.game.input.KeyReader;
import com.krygier.game.utils.DataLoader;

import java.util.Map;

public class Application {

    private static final String FIRST_ROOM = "a0";

    public static void main(String[] args) {
        System.out.println("Welcome to dungeon crawling game.");
        System.out.println("You can move to next room by typing direction - n, e, w or s. Type exit to close application");
        Map<String, Map<String, String>> data = DataLoader.getDataFromFile("data.txt");
        Game game = new Game(new KeyReader());
        game.play(data, FIRST_ROOM);
    }

}
