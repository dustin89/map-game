package com.krygier.game.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataLoader {

    private static final String RESOURCES_PATH = "src/main/resources/";

    public static Map<String, Map<String, String>> getDataFromFile(String fileName) {
        String filePath = RESOURCES_PATH + fileName;
        Map<String, Map<String, String>> map = new HashMap<>();

        String line;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ", 2);
                if (parts.length >= 2) {
                    String room = parts[0];
                    String movesText = parts[1];
                    String[] moves = movesText.split(" ");
                    HashMap<String, String> movesMap = new HashMap<>();
                    for (String moveText : moves) {
                        String[] move = moveText.split(":");
                        String direction = move[0];
                        String availableRoom = move[1];
                        movesMap.put(direction, availableRoom);
                    }
                    map.put(room, movesMap);
                } else {
                    System.out.println("ignoring line: " + line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return map;

    }


}
