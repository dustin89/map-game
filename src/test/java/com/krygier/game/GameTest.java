package com.krygier.game;


import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class GameTest {

    private static final String RESOURCES_PATH = "/expected-output/";
    private static final String FIRST_ROOM = "a0";

    private Game out;

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private TestInputProvider testInputProvider;

    @Before
    public void setUp() {
        testInputProvider = new TestInputProvider();
        out = new Game(testInputProvider);
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void game_correctMoves_printsOutput_exits() {

        testInputProvider.setMoves(asList("n", "n", "e", "w", "exit"));
        out.play(getSampleData(), FIRST_ROOM);

        String expectedOutput = loadFile("correct-moves-output.txt");

        assertThat(outputStreamCaptor.toString()).isEqualTo(expectedOutput);
    }

    @Test
    public void game_notExistingMove_printsInformationAboutIncorrectMove_exits() {

        testInputProvider.setMoves(asList("n", "e", "exit"));
        out.play(getSampleData(), FIRST_ROOM);

        String expectedOutput = loadFile("incorrect-moves-output.txt");

        assertThat(outputStreamCaptor.toString()).isEqualTo(expectedOutput);
    }

    private String loadFile(String fileName) {
        try {
            return new String(Files.readAllBytes(Paths.get(
                    GameTest.class
                            .getResource(RESOURCES_PATH + fileName)
                            .toURI())));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Map<String, Map<String, String>> getSampleData() {


        Map<String, Map<String, String>> data = new HashMap<>();

        Map<String, String> move00 = new HashMap<>();
        move00.put("n", "a3");
        data.put("a0", move00);

        Map<String, String> move01 = new HashMap<>();
        move01.put("e", "a2");
        move01.put("s", "a3");
        data.put("a1", move01);

        Map<String, String> move02 = new HashMap<>();
        move02.put("s", "a5");
        move02.put("w", "a1");
        data.put("a2", move02);

        Map<String, String> move03 = new HashMap<>();
        move03.put("n", "a1");
        move03.put("s", "a0");
        move03.put("w", "a4");
        data.put("a3", move03);

        return data;

    }
}
