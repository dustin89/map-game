package com.krygier.game;

import com.krygier.game.input.InputProvider;

import java.util.List;

public class TestInputProvider implements InputProvider {

    private List<String> moves;
    private int currentIndex = 0;

    @Override
    public String provideInput() {
        String currentMove = moves.get(currentIndex);
        currentIndex++;
        return currentMove;
    }

    public void setMoves(List<String> moves) {
        currentIndex = 0;
        this.moves = moves;
    }
}
